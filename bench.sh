#!/bin/bash

export LD_LIBRARY_PATH=/xonotic/game/d0_blind_id/.libs

do_benchmark() {
    time ./darkplaces/darkplaces-dedicated -xonotic -userdir /xonotic/userdir +serverconfig serverbench.cfg > /dev/null
}

echo "FIRST RUN"
do_benchmark
echo "SECOND RUN"
do_benchmark
echo "THIRD RUN"
do_benchmark
