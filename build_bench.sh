#!/bin/bash

set -x
IMAGE_NAME=morosophos/xonotic-server-benchmark
XONOTIC_ROOT=../xonotic/

cd $XONOTIC_ROOT/data/xonotic-data.pk3dir || exit 1
if [ -n "$(git status --porcelain)" ]; then
  echo "Data dir dirty, please commit or revert changes"
  exit 1
fi

VER=$(git describe --tags --dirty='~')
cd - || exit 1

cp bench.sh "$XONOTIC_ROOT/bench.sh"
cp dockerignore.bench "$XONOTIC_ROOT/.dockerignore"
docker build -t "$IMAGE_NAME:$VER" -f ./Dockerfile.bench --build-arg VER="$VER" --force-rm "$XONOTIC_ROOT"
docker tag "$IMAGE_NAME:$VER" "$IMAGE_NAME:latest"
rm "$XONOTIC_ROOT/.dockerignore"
rm "$XONOTIC_ROOT/bench.sh"
